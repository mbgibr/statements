package mbg.ibr.statements;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBatchProcessing
public class StatementsBatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(StatementsBatchApplication.class, args);
    }

}

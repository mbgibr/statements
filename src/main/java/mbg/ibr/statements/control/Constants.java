package mbg.ibr.statements.control;

public interface Constants {

    String UPDATE_CUSTOMER_NAME = "UPDATE CUSTOMER SET FIRST_NAME = COALESCE(:firstName, FIRST_NAME), " +
            "MIDDLE_NAME = COALESCE(:middleName, MIDDLE_NAME), " +
            "LAST_NAME = COALESCE(:lastName, LAST_NAME) " +
            "WHERE CUSTOMER_ID = :customerId ";

    String UPDATE_CUSTOMER_ADDRESS = "UPDATE CUSTOMER SET " +
            "ADDRESS1 = COALESCE(:address1, ADDRESS1), " +
            "ADDRESS2 = COALESCE(:address2, ADDRESS2), " +
            "CITY = COALESCE(:city, CITY), " +
            "STATE = COALESCE(:state, STATE), " +
            "POSTAL_CODE = COALESCE(:postalCode, POSTAL_CODE) " +
            "WHERE CUSTOMER_ID = :customerId";

    String UPDATE_CUSTOMER_CONTACT = "UPDATE CUSTOMER SET " +
            "EMAIL_ADDRESS = COALESCE(:emailAddress, EMAIL_ADDRESS), " +
            "HOME_PHONE = COALESCE(:homePhone, HOME_PHONE), " +
            "CELL_PHONE = COALESCE(:cellPhone, CELL_PHONE), " +
            "WORK_PHONE = COALESCE(:workPhone, WORK_PHONE), " +
            "NOTIFICATION_PREF = COALESCE(:notificationPreferences, " +
            "NOTIFICATION_PREF) " +
            "WHERE CUSTOMER_ID = :customerId";

    String INSERT_TRANSACTION = "INSERT INTO TRANSACTION (TRANSACTION_ID, ACCOUNT_ACCOUNT_ID, DESCRIPTION, CREDIT, DEBIT, TIMESTAMP) " +
            "VALUES (:transactionId, :accountId, :description, :credit, :debit, :timestamp)";

    String SELECT_TRANSACTION = "select transaction_id, account_account_id, description, credit, debit, timestamp " +
            " from TRANSACTION  " +
            " order by timestamp ";

    String UPDATE_TRANSACTION = "UPDATE ACCOUNT SET  BALANCE = BALANCE + :transactionAmount WHERE ACCOUNT_ID = :accountId";
}
